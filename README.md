```
___  ___   ___   ______   _____   _____   _____   _____ 
|  \/  |  / _ \  | ___ \ /  ___| |_   _| |_   _| |  ___|
| .  . | / /_\ \ | |_/ / \ `--.    | |     | |   | |__  
| |\/| | |  _  | |    /   `--. \   | |     | |   |  __| 
| |  | | | | | | | |\ \  /\__/ /  _| |_    | |   | |___ 
\_|  |_/ \_| |_/ \_| \_| \____/   \___/    \_/   \____/ 
```

      
## Comments
This was my first experience in React. Therefore some things might not have been build in the ideal way, but I am still learning until this day, and I'm sure I would take on some aspects of this application in a new way.

# Marsite, a react app
The year is 2052. The time has come for us humans to destroy yet another planet. This time, Mars -not to be mistaken with the delicious candy bar made by Mars inc- is the victim. People from all over the world have bought their tickets to go and colonise on this land. While all of this is happening, Elon Musk is laughing through his window at his retirement home. But you are probably wondering, all those houses, all those people, how will we manage that? Fear not, for the app 'Marsite' has arrived. Equipped with multiple handy functions, Marsite will guide us through this new adventure, we won't be needing houston no more! So, buckle up and prepare for lift off as we set our course to Mars! 

## Hosted!
You can find the dashboard [here](https://ttsd.ti.howest.be/marsit-62/client/) (url: https://ttsd.ti.howest.be/marsit-62/client/).

## Main functions
#### House related
- See all houses
- See details for 1 house
- Change house details
- Remove a house

###### Leaflet
- Show a map
- Show all houses on the map

#### Request related
- See all requests
- See request details

#### Event related
- See all events
- Change event status

## Communication

#### House
For all things house related, I will be using the [flask api](https://git.ti.howest.be/TI/2020-2021/s5/trending-topics-in-software-development/project/mars-it-07/flask).

#### Request
For all things house related, I will be using the [api from the transaction server](https://git.ti.howest.be/TI/2020-2021/s5/trending-topics-in-software-development/project/mars-it-07/transaction-server).

## Components
For this project, I used some external components made by other React enthusiasts.

#### React Leaflet
To display the map, I used the [React Leaflet component](https://react-leaflet.js.org/).

#### React Circular Progressbar
When showing house details, I wanted to show the state in a progress bar. To do this, I used the [react-circular-progressbar module](https://www.npmjs.com/package/react-circular-progressbar).

#### React Helmet
For all things around the document head, I used the  [React Helmet component](https://github.com/nfl/react-helmet)

## Work in progress
This dashboard app is still a work in progress. I am working on the following things:
 - More house editing functions
 - Authentication
 - CSS overhaul

 >  The great Michelangelo once said: "Genius is eternal patience". So bear with me while I sculpt my David.
