import React from 'react';
import './css/footer.css';

function Footer(){
   return( <footer className="footer">
    <div className="footer__copyright">&copy; Marsite </div>
    <div className="footer__signature"></div>
  </footer>)
}

export default Footer;