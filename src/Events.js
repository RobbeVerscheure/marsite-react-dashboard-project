import React, { useState, useEffect } from 'react';
import { Helmet } from "react-helmet";
import axios from 'axios';
import LoadingGif from './images/loading.gif';
import './css/events.css';

function Events() {
    useEffect(() => {
        getMessage();
        
    }, []);

    const [event, setEvent] = useState([]);

    const [events,setEvents] = useState([]);


    function getMessage(){
        let source = new EventSource('https://ttsd.ti.howest.be/marsit-62/weather-event-simulator/');
        source.onmessage = function (e) {
            events.push(e.data);  
            console.log(e.data);
            setEvent(e.data);
        }  
    }

   
    let eventsList = [];

        let i = 0;
        events.forEach(function(event){
            let eventJson = JSON.parse(event);
            i++;
            eventsList.push(<li key={i} id={eventJson.HouseId} className="eventRow flex"><span><span className="eventHouseName">{eventJson.HouseName}:</span> <span className="eventName">{eventJson.Event}</span></span> <span><span className="eventDamage">{eventJson.Damage}</span> damage </span> <a  onClick={() => confirmEvent(eventJson.HouseId,eventJson.Damage)} className="confirmEvent">Confirm</a></li>)
        })
    
    

        function confirmEvent(id,statusChange){
            let token = localStorage.getItem("json_token");
            axios.patch(`https://ttsd.ti.howest.be/marsit-62/flask/api/houses/${id}/status`,{"status_change": statusChange },{
                headers: {
                  Authorization: 'Bearer ' + JSON.parse(token)
                }
              })
            .then(response => {
                document.getElementById(`${id}`).classList.add("hidden");  
                document.getElementById(`${id}`).classList.remove("flex");       
            })
            .catch(error => {
                console.log(error);              
            })
        }


        if (eventsList.length === 0) {
            return (<div >
                <Helmet>
            <title>Loading events... | Marsite</title>
            </Helmet>
              <figure className="loadingEvents">
              <img src={LoadingGif} alt="loading" />
              <figcaption>Waiting for an event to occur...</figcaption>
              </figure>
              
                     
            </div>)
        } else {
           
            return (<div>
                <Helmet>
            <title>Events | Marsite</title>
            </Helmet>
                <div id="eventOverview">
                    {eventsList}
                </div>
                     
            </div>)
        }

   
}



export default Events;