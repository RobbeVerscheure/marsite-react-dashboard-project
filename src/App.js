import React from 'react';
import './css/App.css';
import './css/dashboard.css';
import Header from './Header';
import Nav from './Nav';
import Houses from './Houses';
import HouseDetails from './houseDetails'
import Requests from './Requests';
import Footer from './Footer';
import Events from './Events';
import RequestDetails from './requestDetails';
import AddHouse from './addHouse';
import EditHouse from './editHouse';
import {Link} from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './logIn';
import {Helmet} from 'react-helmet';

import EventIcon from './images/dashboardEventIcon.png';
import HouseIcon from './images/dashboardHouseIcon.png';
import RequestIcon from './images/dashboardRequestIcon.png';

function App() {
  if (localStorage.getItem("loggedIn") === null) {
    localStorage.setItem('loggedIn',JSON.stringify(false));
  }

  let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));
  

  if(loggedIn){
    
    return (

      <div className="grid-container">
  
        <Header />
  
        <Router basename="/marsit-62/client">
          <Nav />
          <main className="main">
            <Switch>
              <Route path="/" exact component={Dashboard} />
              <Route path="/houses" component={Houses} />
              <Route path="/house/:id" component={HouseDetails} />
              <Route path="/editHouse/:id" component={EditHouse}/>
              <Route path="/requests" component={Requests} /> 
              <Route path="/request/:id" component={RequestDetails} />
              <Route path="/events" component={Events} />
              <Route path="/addHouse" component={AddHouse} />
            </Switch>
          </main>
        </Router>
  
        <Footer />
  
  
      </div>
  
    );
  } else {
    return(<div>
      <Login />
      </div>)
  }

 
}

const Dashboard = () => (
  <div className="page">
  <Helmet>
            <title>Dashboard | Marsite</title>
            </Helmet>
    <div className="dashboardOverview">
    
    <Link className="dashLink" to='/houses'>
    <div className=" dashLinkHouse"><div className="dashLinkCard"><img src= {HouseIcon} alt="House icon"></img></div><h2>HOUSES</h2></div>
    </Link>
    <Link className="dashLink" to='/requests'>
    <div className=" dashLinkRequest" ><div className="dashLinkCard"><img src= {RequestIcon} alt="Request icon"></img></div><h2>REQUESTS</h2></div>
    </Link>
    <Link className="dashLink" to='/events'>
    <div className=" dashLinkEvent" > <div className="dashLinkCard"><img src= {EventIcon} alt="Event icon"></img></div><h2>EVENTS</h2></div>
    </Link>
    </div>


  </div>
);

export default App;
