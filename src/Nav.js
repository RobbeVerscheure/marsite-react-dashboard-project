import React from 'react';
import {Link} from 'react-router-dom';
import Icon from './images/icon.png'
import './css/nav.css';

function Nav(){

return(
    <aside className="sidenav">
      <figure className="navFigure"><img src={Icon} alt="Marsite logo"></img><figcaption>MARSITE</figcaption></figure>
    <ul className="sidenav__list">
    <Link to='/'>
    <li className="sidenav__list-item">Dashboard</li>
    </Link>
    <Link to='/houses'>
    <li className="sidenav__list-item">Houses</li>
    </Link>
    <Link to='/requests'>
    <li className="sidenav__list-item">Requests</li>
    </Link>
    <Link to='/events'>
    <li className="sidenav__list-item">Events</li>
    </Link>
    </ul>
  </aside>
)

}

export default Nav;