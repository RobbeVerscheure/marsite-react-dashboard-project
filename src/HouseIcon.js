import L from 'leaflet';

const iconHouse = new L.Icon({
    iconUrl: require('./images/icon.svg'),
    iconRetinaUrl: require('./images/icon.svg'),
    iconAnchor: null,
    popupAnchor: null,
    shadowUrl: null,
    shadowSize: null,
    shadowAnchor: null,
    iconSize: new L.Point(60, 75),
    className: 'leaflet-div-icon'
});

export { iconHouse };