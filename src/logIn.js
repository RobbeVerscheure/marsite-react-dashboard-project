import React, { useState } from "react";
import axios from 'axios';
import './css/forms.css';
import {Helmet} from 'react-helmet';

export default function Login() {
  const [username, setUsername] = useState("");
  const [loggedIn, setLoggedIn]  = useState("");
  const [password, setPassword] = useState("");

  function validateForm() {
    return username.length > 0 && password.length > 0;
  }

  function logInAuto(){
    let loginData = {"username": "admin", "password": "mars2052"};

    axios.post("https://ttsd.ti.howest.be/marsit-62/flask/api/login/admin",loginData)
        .then((res) => { actionBasedOnResponse(res)})
  }


  function handleSubmit(event) {
    event.preventDefault();
    logIn();
   
  }

 function logIn(){

    let loginData = {"username": username, "password": password}

    axios.post("https://ttsd.ti.howest.be/marsit-62/flask/api/login/admin",loginData)
        .then((res) => { actionBasedOnResponse(res)})

  }

  function actionBasedOnResponse(res){
    if(res.status === 200){
      localStorage.setItem('json_token',JSON.stringify(res.data.json_token));
      localStorage.setItem('loggedIn',JSON.stringify(true));
      setLoggedIn(true);
      window.location.reload();
    } else {
      setLoggedIn(false);
    }
  }

  let message = "";

  if(loggedIn){
    message = "You are logged in!"
  } else {
    message = "You are not logged in yet!"
  }
  

  return (

    <div className="Login form-style-10">
              <Helmet>
            <title>Login | Marsite</title>
            </Helmet>
      <form onSubmit={handleSubmit}>



        <p>Here for the peer review? <a className="autoLog" onClick={logInAuto}>CLICK ME!</a></p>

        <p>{message}</p>

          <label>Name</label>
          <input type="text" onChange={(e) => setUsername(e.target.value)}></input>
       
          <label>Password</label>
          <input type="password" onChange={(e) => setPassword(e.target.value)}></input>
       
          <input type="submit"></input>

      </form>
    </div>
  );
}