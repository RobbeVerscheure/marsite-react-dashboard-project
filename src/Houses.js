import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Mapped from './Mapped';
import axios from 'axios';
import Login from './logIn';
import EditIcon from './images/edit.png';
import DeleteIcon from './images/delete.png';
import './css/houses.css';
import {Helmet} from 'react-helmet';

function Houses() {

    useEffect(() => {
        fetchSectors();
    }, []);

    const [sectors, setSectors] = useState([]);


    const fetchSectors = async () => {
        const data = await fetch(
            "https://ttsd.ti.howest.be/marsit-62/flask/api/sectors/details"
        );

        const items = await data.json();
       
      setSectors(items);
    }

    let token = localStorage.getItem("json_token");
    localStorage.setItem("created",JSON.stringify(0));

    let DOMListSector1 = [];
    let DOMListSector2 =[];
    let DOMListSector3 = [];
    let DOMListSector4 = [];
    
    let i = 1;
    sectors.forEach(function (sector) {
        let tempList = [];
        sector.addresses.forEach(function (address) {
        tempList.push( <li className="houseLink"><Link to={`/house/${address.house.id}`}>{address.house.house_name}</Link> 
                       <div className="actionButtons"> <Link to={`/editHouse/${address.house.id}`}>
                       <img className="editButton" alt="Edit icon" src={EditIcon}></img></Link> 
                       <img className="deleteButton" onClick={() => deleteHouse(address.house.id)} alt="Delete icon" src={DeleteIcon}></img></div> </li>);
        })

        switch(i){
            case 1:
                DOMListSector1 = tempList;
                break;
            case 2:
                DOMListSector2 = tempList;
                break;
            case 3:
                DOMListSector3 = tempList;
                break;
            case 4:
                DOMListSector4 = tempList;    
                break;
            default:
                DOMListSector1 = tempList;
                break;
        }
        i++;
        
    })


    function createRandomHouse(){
        axios.post("https://ttsd.ti.howest.be/marsit-62/flask/api/houses/random",{},{
            headers: {
              Authorization: 'Bearer ' + JSON.parse(token)
            }
          })
        .then(response => {
            fetchSectors();       
        })
        .catch(error => {
            console.log(error);    
        })
    }

    function deleteHouse(houseId){

        axios.delete(`https://ttsd.ti.howest.be/marsit-62/flask/api/houses/${houseId}`,{
            headers: {
              Authorization: 'Bearer ' + JSON.parse(token)
            }})
        .then(response => {
            fetchSectors();
        })
        .catch(error => {
            console.log(error); 
        })


    }


    if (localStorage.getItem("loggedIn") === null) {
        localStorage.setItem('loggedIn',JSON.stringify(false));
      }
    
      let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));
  
      if(loggedIn){
        return (

            <div className="page">     
            <Helmet>
            <title>Houses | Marsite</title>
            </Helmet>     
                <Mapped data={sectors} />
                <div className="addButtons">
                <button onClick={createRandomHouse} className="addHouseButton">Generate random house</button>
                <Link to={`/addHouse`}><button className="addHouseButton">Create new house</button></Link>
                </div>
               
               
                  <div className="houseOverview">
                     <div className="sectorView sectorView1" >
                         <h2>Elon's Crib</h2>
                         <ul id="sector1">
                             {DOMListSector1}
                         </ul>
                     </div>
                     <div className="sectorView sectorView2">
                         <h2>Tesla World</h2>
                         <ul id="sector2">{DOMListSector2}</ul>
                     </div>
                     <div className="sectorView sectorView3">
                         <h2>Hyperloop</h2>
                         <ul id="sector3">{DOMListSector3}</ul>
                     </div>
                     <div className="sectorView sectorView4">
                         <h2>S3XY</h2>
                         <ul id="sector4">{DOMListSector4}</ul>
                     </div>
                     </div>
               
            </div>
        )
      } else {
          <Login></Login>
      }

   
}

export default Houses;