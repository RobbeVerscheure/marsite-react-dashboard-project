import React, {Component} from 'react';
import axios from 'axios';
import Login from './logIn';
import './css/addHouse.css';
import './css/forms.css';
import {Helmet} from "react-helmet";
import { Redirect } from 'react-router-dom';
import Houses from './Houses.js'
import { Link } from 'react-router-dom';

class AddHouse extends Component{

    constructor(props){
        super(props)

        this.state={
            house_name: '',
            sector_id: 1,
            capacity: 0,
            created: 0,
            sectors: []
        }
    }

    
    changeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    changeIntHandler = (e) => {
        this.setState({[e.target.name]: parseInt(e.target.value)})
    }


    submitHandler = (e)=>{
        e.preventDefault();
        let token = localStorage.getItem("json_token");
        let house = {"house_name": this.state.house_name,"sector_id": this.state.sector_id,"capacity": this.state.capacity};
        axios.post("https://ttsd.ti.howest.be/marsit-62/flask/api/houses",house,{
            headers: {
              Authorization: 'Bearer ' + JSON.parse(token)
            }
          })
        .then(response => {
           document.querySelector("#houseCreated").classList.remove("hidden");
            
        })
        .catch(error => {
            this.created = 1;
            document.querySelector("#houseNotCreated").classList.remove("hidden");
        })
    }

render(){
    const { houseName,sectorId,houseCapacity} = this.state;

    if (localStorage.getItem("loggedIn") === null) {
        localStorage.setItem('loggedIn',JSON.stringify(false));
      }

      let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));
    if(loggedIn){
        
            return( <div>
                <Helmet>
                <title>Add a house | Marsite</title>
                </Helmet>
    
                <form className="addHouseForm form-style-10" onSubmit={this.submitHandler}>
                    <h3>Add a new house!</h3>
        
                    <label htmlFor="houseName">Name:</label>
                    <input id="houseName" name="house_name" type="text" value={houseName} onChange={this.changeHandler}></input>
                    <label htmlFor="sectorId">Sector:</label>
                    <select id="sectorId" name="sector_id" value={sectorId} onChange={this.changeIntHandler}>
                        <option selected value={1}>Elon's Crib</option>
                        <option value={2}>Tesla World</option>
                        <option value={3}>Hyperloop</option>
                        <option value={4}>S3XY</option>
                    </select>
                    <label htmlFor="houseCapacity">Capacity:</label>
                    <input id="houseCapacity" name="capacity" type="number" min={1} max={6} value={houseCapacity} onChange={this.changeIntHandler}/>
                  
                    <input type="submit" value={"Let's build " + this.state.house_name + "!"} ></input>
                </form>
                <div id="houseCreated" className="hidden"><p>{this.state.house_name} has been created! <Link to="/houses"><a >See overview</a></Link> or <a onClick={() => {window.location.reload()}}>create another house</a></p></div>
                <div id="houseNotCreated" className="hidden"><p>{this.state.house_name} could not be created. Try again!</p></div>
                </div>
          )
        
        
    } else {
        return(
            <Login></Login>
        )
       
    }
 }
}
 export default AddHouse;

