import React from 'react';
import marsIcon from './images/houseIcon.png'
import './css/map.css';
import L from 'leaflet';
import { TileLayer, Marker, Popup, MapContainer, Polygon} from 'react-leaflet';

class Mapped extends React.Component {

   render(){

    let ownIcon = L.icon({
        iconUrl: marsIcon,
        iconSize: [25, 25], // size of the icon
    });
    

    
    let southWest = L.latLng(48, 13);
    let northEast = L.latLng(50, 15);
    let boundsToSet = L.latLngBounds(southWest, northEast);

    let sectors = this.props.data;

    let houseMarks = [];
    let i =1;
    sectors.forEach(function (sector) {
        
        let classToBeAdded = "";
        switch(i){
            case 1:
                classToBeAdded = "poly1";
                break;
            case 2:
                classToBeAdded = "poly2";
                break;
            case 3:
                classToBeAdded = "poly3";
                break;
            case 4:
                classToBeAdded = "poly4";    
                break;
            default:
                classToBeAdded = "poly1";
                break;
        }
        
        



        houseMarks.push(  <Polygon className={classToBeAdded} color="red" positions={[[sector.sector_range.long_min,sector.sector_range.lat_max],[sector.sector_range.long_min,sector.sector_range.lat_min],[sector.sector_range.long_max,sector.sector_range.lat_min],[sector.sector_range.long_max,sector.sector_range.lat_max]]}>
        <Popup>
            {sector.name}
        </Popup>
        </Polygon>)

        sector.addresses.forEach(function (address) {
        houseMarks.push( <Marker icon={ownIcon} position={[address.long, address.lat]}>
            <Popup>
              {address.house.house_name}
            </Popup>
        </Marker>);
        })
        i++;
    })

    
    return (
        <div >
        <MapContainer id="map" center={[49, 14]} zoom={7} minZoom={7} maxBounds={boundsToSet} zoomControl={true} dragging={true} scrollWheelZoom={true}>
            <TileLayer
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                url=" http://c.tiles.wmflabs.org/hillshading/{z}/{x}/{y}.png"
            />
           

            {houseMarks}
           

        </MapContainer>
    </div>
   

    )
    }
}

export default Mapped;