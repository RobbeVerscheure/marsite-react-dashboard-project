import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import {Helmet} from 'react-helmet';
import Login from './logIn';
import './css/requests.css';

function Requests(){
    useEffect(() => {
        fetchSectors();
    }, []);

    const [requests, setRequests] = useState([]);


    const fetchSectors = async () => {
        const data = await fetch(
            "https://ttsd.ti.howest.be/marsit-62/transactions/purchases"
        );

        const items = await data.json();
        setRequests(items);
    }

    function getDate(date){
        return date.substring(0,10);
    }

    if (localStorage.getItem("loggedIn") === null) {
        localStorage.setItem('loggedIn',JSON.stringify(false));
      }
    
      let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));
  
      if(loggedIn){
        if(requests !== undefined){
            return (
                <div className="page">
                    <Helmet>
            <title>Requests | Marsite</title>
            </Helmet>
                     <ul className="requestList">
                     {requests.map((purchase) => {     
                            return (<li className="requestRow" key={"purchase"+purchase.id}><Link to={`/request/${purchase.id}`}>Nr {purchase.id} </Link><span className="requestDate">{getDate(purchase.date)}</span></li>) 
                    })}
                </ul>
                </div>
              
            )
        } else {
            return(<h1>Loading...</h1>)
        }
      } else {
          <Login></Login>
      }


   
}

export default Requests;