import React, { useState, useEffect} from 'react';
import { CircularProgressbar } from 'react-circular-progressbar';
import 'react-circular-progressbar/dist/styles.css';
import Login from './logIn';
import PersonIcon from './images/personIcon.png';
import './css/houseDetails.css';
import {Helmet} from 'react-helmet';


function House( {match}){

    if (localStorage.getItem("loggedIn") === null) {
        localStorage.setItem('loggedIn',JSON.stringify(false));
      }

      let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));

useEffect(()=>{
    fetchHouse();
    
},[]);

const [house, setHouse] = useState({});

const fetchHouse = async () => {
   const data = await fetch(`https://ttsd.ti.howest.be/marsit-62/flask/api/houses/${match.params.id}`)
    
   const house = await data.json();
   
   setHouse(house);

}

if(loggedIn){
    if(house.address !== undefined){

        let personArray = [];
        let partsArray = [];
        for(let i = 0; i < house.capacity; i++){
            personArray.push(<img src={PersonIcon} className="personIcon" alt="PersonIcon" />)
        }
        
        for(let j = 0; j < house.parts.length; j++){
            partsArray.push(<li className="partRow">{house.parts[j].name}</li>)
        }

        return(
            <div className="detailOverview">
                <Helmet>
            <title>Details {house.house_name} | Marsite</title>
            </Helmet>
                
                <div className="houseDetails">
                <h1 className="houseName">{house.house_name}</h1>
                <h2 className="sectorName">{house.address.sector.name}</h2>
    
                <p>Status: <div className="statusPercentage"><CircularProgressbar className="statusPercentage" value={house.status} text={`${house.status}%`} /></div></p>
                <p>Capacity: {personArray}  </p>
                <p>Longitude: <span className="locValue">{house.address.long}</span></p>
                <p>Latitude: <span className="locValue">{house.address.lat}</span></p>
    
                <p>Parts:</p>
                <ul className="partList">
                    {partsArray}
                </ul>
            </div>
            </div>
           
        )
    }
    else {
        return(<div><h1>Fetching data....</h1></div>)
    }      
} else {

    return(<Login></Login>)
    
}


  
   
}


export default House;