import React, { Component} from 'react';
import Login from './logIn';
import { Link } from 'react-router-dom';
import axios from 'axios';
import './css/editHouse.css';
import './css/forms.css';
import Helmet from 'react-helmet';

class EditHouse extends Component{
    constructor(props){
        super(props)

        this.state={
            house_name: 'hoi',
            sector_id: 0,
            capacity: 0,
          
            status:0,
            created: 0,
            sectors: []
        }
    }

     
    changeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    componentDidMount() {
        this.fetchHouse();
      }
    changeIntHandler = (e) => {
        this.setState({[e.target.name]: parseInt(e.target.value)})
    }

    changeFloatHandler = (e) => {
        this.setState({[e.target.name]: parseFloat(e.target.value)})
    }


    fetchHouse = async () => {
       const data = await fetch(`https://ttsd.ti.howest.be/marsit-62/flask/api/houses/${this.props.match.params.id}`)
        
       const house = await data.json();
    
      this.setState({
        house_name: house.house_name
      });
    
       this.setState({"sector_id": house.address.sector.id});
       this.setState({"capacity": house.capacity});
       this.setState({"status": house.status});
      
    }

    submitHandler = (e)=>{
        e.preventDefault();
        let house = {"house_name": this.state.house_name,"sector_id": this.state.sector_id,"capacity": this.state.capacity, "status":this.state.status};
        let token = localStorage.getItem("json_token");
        axios.put(`https://ttsd.ti.howest.be/marsit-62/flask/api/houses/${this.props.match.params.id}`,house,{
            headers: {
              Authorization: 'Bearer ' + JSON.parse(token)
            }
          })
        .then(response => {
            this.setState({"created": 1});
            document.querySelector("#houseEdited").classList.remove("hidden");

        })
        .catch(error => {
            this.setState({"created": 0});
            document.querySelector("#houseNotEdited").classList.remove("hidden");
        })
    }

   
    render(){
    if (localStorage.getItem("loggedIn") === null) {
        localStorage.setItem('loggedIn',JSON.stringify(false));
      }

      let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));

      if(loggedIn){

        return(
            <div>
                <Helmet>
            <title>Edit house | Marsite</title>
            </Helmet>
        <form className="editHouseForm form-style-10" onSubmit={this.submitHandler}>
             <h3><span class="editSpan">EDIT</span> {this.state.house_name}</h3>
             
    
    <label htmlFor="houseName">Name:</label>
    <input id="houseName" name="house_name" type="text" value={this.state.house_name} onChange={this.changeHandler}></input>
    <label htmlFor="sectorId">Sector:</label>
    <select id="sectorId" name="sector_id" value={this.state.sector_id} onChange={this.changeIntHandler}>
                    <option value={1}>Elon's Crib</option>
                    <option value={2}>Tesla World</option>
                    <option value={3}>Hyperloop</option>
                    <option value={4}>S3XY</option>
    </select>
    <label htmlFor="houseCapacity">Capacity:</label>
    <input id="houseCapacity" name="capacity" type="number" min={1} max={6} value={this.state.capacity} onChange={this.changeIntHandler}/>
    <label htmlFor="houseStatus">Status:</label>
    <input id="houseStatus" name="status" type="number" min={0} max={100} value={this.state.status} onChange={this.changeIntHandler}/>

   

    <input type="submit"></input>
        </form>

        <div id="houseEdited" className="hidden"><p>{this.state.house_name} has been edited! <Link to="/houses"><a >See overview</a></Link></p></div>
        <div id="houseNotEdited" className="hidden"><p>{this.state.house_name} could not be edited. Try again!</p></div>
                
        </div>)



      } else {
          return(<Login></Login>)
      }
    }

}

export default EditHouse;