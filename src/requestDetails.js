import React, { useState, useEffect} from 'react';
import { Helmet } from "react-helmet";
import Login from './logIn';
import RequestHouse from './images/requestHouse.png';
import RequestID from './images/requestId.png';
import './css/requestDetails.css';

function Request({match}){
    useEffect(()=>{
        fetchPurchase();
      
    },[]);
    
    const [request, setRequest] = useState({});
    const [house, setHouse] = useState({});
    const [housePart, setHousePart] = useState({});
    
    const fetchPurchase = async () => {
       const data = await fetch(`https://ttsd.ti.howest.be/marsit-62/transactions/purchases/${match.params.id}`)
        
       const request = await data.json();
      
       setRequest(request);
       fetchHouse(request.houseId);
       fetchParts(request.housePartId);
    
    }
    const fetchHouse = async (id) => {
        console.log(">Fetching house");
        const data = await fetch(`https://ttsd.ti.howest.be/marsit-62/flask/api/houses/${id}`)
         
        const house = await data.json();
        setHouse(house);
     }

     const fetchParts = async (id) => {
        const data = await fetch(`https://ttsd.ti.howest.be/marsit-62/flask/api/parts`)
         
        const parts = await data.json();

        parts.forEach(part => {
            if(part.id === id){
                setHousePart(part);
            }
        });        
     }
     if (localStorage.getItem("loggedIn") === null) {
        localStorage.setItem('loggedIn',JSON.stringify(false));
      }
    
      let loggedIn = JSON.parse(localStorage.getItem('loggedIn'));

      if(loggedIn){

      if(request !== undefined){
          
        return(<div>
            <Helmet>
            <title>Details request | Marsite</title>
            </Helmet>
            <div className="requestInfo">
                <figure className="requestHouse">
                <img src={RequestHouse} alt="House icon" />
                <figcaption>{house.house_name}</figcaption>
                </figure>
                <figure className="requestId">
                <img src={RequestID} alt="Id icon" />
                <figcaption>{request.id}</figcaption>
                </figure>
            </div>

            <div  className="requestView">
             <table>
        <thead>
            <tr>
                <th>Item</th>
                <th>Price</th>
            </tr>
        </thead>
        <tbody>
        <tr>
            <td>{housePart.name}</td>
            <td>{housePart.price}</td>
        </tr>
        </tbody>
        </table>
        </div>

             
        </div>)
      } else {
        <div>
            <p>Request is loading...</p>
        </div>
      }
} else {
    <Login></Login>
}  
}
export default Request;